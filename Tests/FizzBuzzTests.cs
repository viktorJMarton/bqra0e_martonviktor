﻿using BeadandoFeladat2021.MainPrograms;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeadandoFeladat2021.Tests
{


    [TestFixture]
    public class FizzBuzzTests
    {
        [Test]

        public void GetOutPut_ShouldWorkFizz()
        {
            //Arrange

            var number =3;

            FizzBuzz testObj = new FizzBuzz();

            //Act

            var outputString = testObj.GetOutput(number);

            //Assert

            Assert.AreEqual("Fizz", outputString);



        }

        public void GetOutPut_ShouldWorkBuzz()
        {
            //Arrange

            var number = 5;

            FizzBuzz testObj = new FizzBuzz();

            //Act

            var outputString = testObj.GetOutput(number);

            //Assert

            Assert.AreEqual("Buzz", outputString);



        }

        [Test]
        public void GetOutPut_ShouldWorkFizzBuzz()
        {
            //Arrange

            var number = 15;

            FizzBuzz testObj = new FizzBuzz();

            //Act

            var outputString = testObj.GetOutput(number);

            //Assert

            Assert.AreEqual("FizzBuzz", outputString);



        }



        [Test]
        public void GetOutPut_ShouldReturnGivenNumber()
        {
            //Arrange

            var number = 7;

            FizzBuzz testObj = new FizzBuzz();

            //Act

            var outputString = testObj.GetOutput(number);

            //Assert

            Assert.AreEqual(number.ToString(), outputString);



        }

     
        

    }



}


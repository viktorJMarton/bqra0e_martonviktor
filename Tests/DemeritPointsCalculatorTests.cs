﻿using BeadandoFeladat2021.MainPrograms;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BeadandoFeladat2021.Tests
{
    [TestFixture]
    public class DemeritPointsCalculatorTests
    {

        [Test]

        public void CalculateDemeritPoints_ShouldWork1()
        {
            //Arrange

            var speed = 60;

            DemeritPointsCalculator testObj= new DemeritPointsCalculator();

            //Act

            var demeritPoints = testObj.CalculateDemeritPoints(speed);

            //Assert

            Assert.AreEqual(1, demeritPoints);



        }


        public void CalculateDemeritPoints_ShouldWork2()
        {
            //Arrange

            var speed = 70;

            DemeritPointsCalculator testObj = new DemeritPointsCalculator();

            //Act

            var demeritPoints = testObj.CalculateDemeritPoints(speed);

            //Assert

            Assert.AreEqual(2, demeritPoints);



        }


        [Test]
        public void CalculateDemeritPoints_ShouldWork_NotEqual()
        {
            //Arrange

            var speed = 60;

            DemeritPointsCalculator testObj = new DemeritPointsCalculator();

            //Act

            var demeritPoints = testObj.CalculateDemeritPoints(speed);

            //Assert

            //Assert.AreEqual(2, demeritPoints);
            Assert.AreNotEqual(2, demeritPoints);



        }

        [Test]
        public void CalculateDemeritPoints_ShouldThrowExp()
        {
            //Arrange

            var speed = 200;

            DemeritPointsCalculator testObj = new DemeritPointsCalculator();

            //Act

            //var demeritPoints = testObj.CalculateDemeritPoints(speed);

            //Assert


            Assert.Throws<ArgumentOutOfRangeException>(() =>testObj.CalculateDemeritPoints(speed));



        }





        [Test]
        public void CalculateDemeritPoints_ShouldreturnNull()
        {
            //Arrange

            var speed = 45;

            DemeritPointsCalculator testObj = new DemeritPointsCalculator();

            //Act

            var demeritPoints = testObj.CalculateDemeritPoints(speed);

            //Assert


            Assert.AreEqual(0, demeritPoints);



        }






    }
}

﻿using BeadandoFeladat2021.MainPrograms;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using BeadandoFeladat2021.MainPrograms;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace BeadandoFeladat2021.Tests
{


    [TestFixture]
    public class NumberToWordsTests
    {

        [Test]

        public void NumToWord_ConvNums_ShoudWork1()
        {

            //Arrange

            var num = 23;

          

            //Act

            var result = NumberToWords.convertNumbers(num);

            //Asset
            Assert.AreEqual("twenty-three", result);

        }


        [Test]

        public void NumToWord_ConvNums_ShoudWork2()
        {

            //Arrange

            var num = 123;



            //Act

            var result = NumberToWords.convertNumbers(num);

            //Asset
            Assert.AreEqual("one hundred and twenty-three", result);

        }

        [Test]
        public void NumToWord_ConvNums_ShoudWork3()
        {

            //Arrange

            var num = 1234;



            //Act

            var result = NumberToWords.convertNumbers(num);

            //Asset
            Assert.AreEqual("one thousand two hundred and thirty-four", result);

        }



        [Test]
        public void NumToWord_ConvNums_ShoudWork4()
        {

            //Arrange

            var num = -1234;



            //Act

            var result = NumberToWords.convertNumbers(num);

            //Asset
            Assert.AreEqual("mimus one thousand two hundred and thirty-four", result);

        }



        [Test]
        public void NumToWord_ConvNums_ShoudNotEqualMimus()
        {

            //Arrange

            var num = -1;



            //Act

            var result = NumberToWords.convertNumbers(num);

            //Asset
            Assert.AreNotEqual("minus one", result);

        }




    }
}

﻿using BeadandoFeladat2021.MainPrograms;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeadandoFeladat2021.Tests
{

    [TestFixture]
    internal class LeapYearTests
    {


        [Test]

        public void CheckLeapYear_ShouldWork1True()
        {
            //Arrange

            var year = 2012;
            LeapYear testObj = new LeapYear();

            //Act

            var isLeapYear = testObj.checkLeapYear(year);

            Assert.IsTrue(isLeapYear);



        }



        [Test]

        public void CheckLeapYear_ShouldWork2False()
        {
            //Arrange

            var year = 2013;
            LeapYear testObj = new LeapYear();

            //Act

            var isLeapYear = testObj.checkLeapYear(year);

            Assert.IsFalse(isLeapYear);



        }


       
    }
}

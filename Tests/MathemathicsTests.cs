﻿using BeadandoFeladat2021.MainPrograms;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeadandoFeladat2021.Tests
{
    [TestFixture]
    public class MathemathicsTests
    {



        [Test]

        public void Mathematics_Add_ShouldWork1()
        {
            //Arrange

            var a = 60;
            var b = 60;

           Mathemathics testObj = new Mathemathics();

            //Act

            var result = testObj.Add(a,b);

            //Assert

            Assert.AreEqual(120, result);



        }


        [Test]

        public void Mathematics_Add_ShouldWork2()
        {
            //Arrange

            var a = 0;
            var b = 0;

            Mathemathics testObj = new Mathemathics();

            //Act

            var result = testObj.Add(a, b);

            //Assert

            Assert.AreEqual(0, result);



        }

        [Test]

        public void Mathematics_Add_ShouldWork3()
        {
            //Arrange

            var a = -5;
            var b = 0;

            Mathemathics testObj = new Mathemathics();

            //Act

            var result = testObj.Add(a, b);

            //Assert

            Assert.AreEqual(-5, result);



        }

        [Test]
        public void Mathematics_Add_ShouldWork4()
        {
            //Arrange

            var a = -5;
            var b = -5;

            Mathemathics testObj = new Mathemathics();

            //Act

            var result = testObj.Add(a, b);

            //Assert

            Assert.AreEqual(-10, result);



        }

        [Test]
        public void Mathematics_Max_ShouldWork1()
        {
            //Arrange

            var a = 10;
            var b = 1;

            Mathemathics testObj = new Mathemathics();

            //Act

            var result = testObj.Max(a, b);

            //Assert

            Assert.AreEqual(a, result);



        }



        [Test]
        public void Mathematics_Max_ShouldWork2()
        {
            //Arrange

            var a = 1;
            var b = 10;

            Mathemathics testObj = new Mathemathics();

            //Act

            var result = testObj.Max(a, b);

            //Assert

            Assert.AreEqual(b, result);



        }

        [Test]
        public void Mathematics_Max_ShouldWorkEqual()
        {
            //Arrange

            var a = 10;
            var b = 10;

            Mathemathics testObj = new Mathemathics();

            //Act

            var result = testObj.Max(a, b);

            //Assert
            Assert.AreEqual(b, result);



        }


        [Test]
        public void Mathematics_GetOddNumbers_ShouldWork1()
        {
            //Arrange

            var limit = 1000;

            Mathemathics testObj = new Mathemathics();

            //Act

            var result = testObj.GetOddNumbers(limit);

            //Assert
            foreach (var i in result)
            {

                bool isOdd = (i % 2 != 0);

                Assert.IsTrue(isOdd);
            }
           



        }


    }
}
